# TRANSLATES

To generate file '**template.pot**', did use terminal command:

```bash
cd minertrade
xgettext -n *.lua -L Lua --force-po --keyword=modMinerTrade.translate --from-code=UTF-8 -o ./locale/template.pot

```

To translate '**template.pot**' to your language use app poedit.:


### Cria arquivo .po a partir de um arquivo de templante .pot

Sintaxe:

```bash
$ msginit --no-translator --no-wrap --locale=$LANG.UTF-8 --output-file=$LANG.po --input=$POT
```

### Atualiza arquivo ,po a partir de um arquivo de templante .pot

Sintaxe:
```bash
$ msgmerge --sort-output --no-wrap --update --backup=off $LANG.po $POT
```

Example:
```bash
msgmerge --sort-output --no-wrap --update --backup=off ./locale/pt_BR.po ./locale/template.pot
```

### Install the PoEdit:

```bash
sudo apt-get install poedit
```

* Locales used: ca;cs;da;de;dv;eo;es;et;fr;hu;id;it;ja;jbo;kn;lt;ms;nb;nl;pl;pt;pt_BR;ro;ru;sl;sr_Cyrl;sv;sw;tr;uk


### Exemple of enable the portuguese language in minetest:

Translate Sample: 

* "locale/pt.po" to portuguese language.

To enable the translate to portuguese language, write "language = pt" in file "minetest.conf". Or write the command ```/set -n language pt``` in game chat, and run again the minetest game.

----

> See more: 
* https://forum.minetest.net/viewtopic.php?f=47&t=21974
* https://github.com/minetest/minetest/issues/8158
* https://gist.githubusercontent.com/mamchenkov/3690981/raw/8ebd48c2af20c893c164e8d5245d9450ad682104/update_translations.sh
