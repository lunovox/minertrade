local modPath = minetest.get_modpath(minetest.get_current_modname())

dofile(modPath.."/config.lua") -- <== Antes de carregar tudo!
dofile(modPath.."/translate.lua") -- <== Antes de 'api.lua'!
dofile(modPath.."/api.lua")
dofile(modPath.."/item_paper_written.lua")
dofile(modPath.."/item_exchange_table.lua")
dofile(modPath.."/item_dispensing_machine.lua")
dofile(modPath.."/item_atm.lua")
--dofile(modPath.."/item_strongbox.lua")
--dofile(modPath.."/item_strongbox_old.lua")
dofile(modPath.."/item_minercash.lua")
dofile(modPath.."/item_computing_app.lua")
--dofile(path.."/item_rent_door.lua")
dofile(modPath.."/api_payday.lua")
--dofile(modPath.."/commands.lua")

--if modMinerTrade.doBankLoad ~= nil then
	modMinerTrade.doBankLoad()
--end

minetest.log('action',"["..modMinerTrade.modName:upper().."] Loaded!")
